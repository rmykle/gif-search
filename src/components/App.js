import React, { Component } from 'react';
import SearchBar from './SearchBar';
import GifList from './GifList';
import axios from 'axios';
import _ from 'lodash';
import Modal from './Modal';


export default class App extends Component {
    constructor() {
        super();
        this.state = {
            gifs: [
                
            ]
        }

        this.handleTermChange = _.debounce(this.handleTermChange, 1000);
        
    }

    handleTermChange(term) {
      const url = `http://api.giphy.com/v1/gifs/search?q=${term}&limit=9&rating=R&api_key=dc6zaTOxFJmzC`;
      const request = axios.get(url)
      .then((respons)=>{
            this.setState({gifs: respons.data.data})
      })
      .catch((error) => {
          console.log(error);
      })
    }

    render() {
        return (
            <div className="container">
                <Modal/>
                <SearchBar onTermChange={this.handleTermChange.bind(this)}/>
                <GifList gifs={this.state.gifs}/>
                
        
            </div>
            )
    }
    
}