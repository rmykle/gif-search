import React from 'react';
import GifItem from './GifItem';


export default (props) => {
    
    return (
        <div className="columns is-multiline is-centered">{createGifs(props.gifs)}</div>
    )
}

function createGifs(gifs) {
    return gifs.map((gif) => {
        return <GifItem key={gif.id} gif={gif}/>
    });
}

