import React from 'react';

export default (props) => {
    console.log(props.gif.images.downsized.url);
    return (
        <div className="column is-one-third gif"> 
            <img src={props.gif.images.downsized.url}/>
        </div>
    )
}