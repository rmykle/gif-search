import React, { Component } from 'react';

class SearchBar extends Component {
    constructor() {
        super();
        this.state = {
            term: ''
        }
    }

    onChange(event) {
        const {value} = event.target;

        this.setState({term: value})
        this.props.onTermChange(value);
    }

    render() {
        return (
            <div className="searchBar">
                <input className="input is-medium" value={this.state.term} onChange={this.onChange.bind(this)} />
            </div>
        );
    }
}

export default SearchBar;